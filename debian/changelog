ros-rosdistro (1.0.1-2) unstable; urgency=medium

  * Remove setuptools runtime dependency (Closes: #1083755)

 -- Timo Röhling <roehling@debian.org>  Wed, 04 Dec 2024 21:52:54 +0100

ros-rosdistro (1.0.1-1) unstable; urgency=medium

  [ Jochen Sprickerhof ]
  * Drop unused build dependency

  [ Timo Röhling ]
  * New upstream version 1.0.1
  * Refresh patches (no functional changes)

 -- Timo Röhling <roehling@debian.org>  Sat, 23 Nov 2024 09:08:31 +0100

ros-rosdistro (0.9.1-2) unstable; urgency=medium

  * Wrap and sort Debian package files
  * Add myself to uploaders

 -- Timo Röhling <roehling@debian.org>  Tue, 24 Sep 2024 15:42:19 +0200

ros-rosdistro (0.9.1-1) unstable; urgency=medium

  * New upstream version 0.9.1
  * Rediff patches
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 18 Jun 2024 16:21:02 +0200

ros-rosdistro (0.9.0-5) unstable; urgency=medium

  * Drop distutils usage

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 30 Jan 2024 08:34:40 +0100

ros-rosdistro (0.9.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.
  * Update standards version to 4.6.2, no changes needed.

  [ Jochen Sprickerhof ]
  * Fix debian rosdistro creation

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 20 Jul 2023 17:55:35 +0200

ros-rosdistro (0.9.0-3) unstable; urgency=medium

  * Switch to pytest and use autopkgtest for real

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 11 Dec 2022 09:49:01 +0100

ros-rosdistro (0.9.0-2) unstable; urgency=medium

  * Enable autopkgtest-pkg-pybuild

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 10 Dec 2022 12:29:49 +0100

ros-rosdistro (0.9.0-1) unstable; urgency=medium

  * New upstream version 0.9.0
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 15 Jun 2022 21:42:35 +0200

ros-rosdistro (0.8.3-2) unstable; urgency=medium

  * Drop gitlab-ci
  * Drop old Breaks/Replaces
  * Add debian rosdistro
  * Bump policy versions (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 17 Feb 2022 23:29:09 +0100

ros-rosdistro (0.8.3-1) unstable; urgency=medium

  * New upstream version 0.8.3

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 11 Oct 2020 13:25:38 +0200

ros-rosdistro (0.8.2-1) unstable; urgency=medium

  * New upstream version 0.8.2
  * update copyright
  * Remove Thomas from Uploaders, thanks for working on this
  * bump policy and debhelper versions (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 Jun 2020 08:52:58 +0200

ros-rosdistro (0.8.0-1) unstable; urgency=medium

  * simplify d/watch
  * New upstream version 0.8.0

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 12 Dec 2019 17:49:28 +0100

ros-rosdistro (0.7.5-1) unstable; urgency=medium

  * New upstream version 0.7.5
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Oct 2019 09:09:43 +0200

ros-rosdistro (0.7.4-2) unstable; urgency=medium

  * Drop Python 2 package (Closes: #938396)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 28 Sep 2019 13:22:11 +0200

ros-rosdistro (0.7.4-1) unstable; urgency=medium

  * New upstream version 0.7.4
  * Bump policy version (no changes)
  * switch to debhelper-compat and debhelper 12
  * add Salsa CI

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 28 Jul 2019 20:35:15 +0200

ros-rosdistro (0.7.2-1) unstable; urgency=medium

  * New upstream version 0.7.2

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 28 Jan 2019 08:12:52 +0100

ros-rosdistro (0.7.1-1) unstable; urgency=medium

  * New upstream version 0.7.1
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 13 Jan 2019 11:25:08 +0100

ros-rosdistro (0.7.0-1) unstable; urgency=medium

  * New upstream version 0.7.0
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 07 Nov 2018 21:13:16 +0100

ros-rosdistro (0.6.9-1) unstable; urgency=medium

  * http -> https
  * New upstream version 0.6.9
  * Bump policy version (no changes)
  * Fix ancient-python-version-field

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 19 Aug 2018 17:14:23 +0200

ros-rosdistro (0.6.8-1) unstable; urgency=medium

  * New upstream version 0.6.8
  * Update copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 31 Mar 2018 13:16:59 +0200

ros-rosdistro (0.6.6-1) unstable; urgency=medium

  * Update Vcs URLs to salsa.d.o
  * New upstream version 0.6.6
  * Update packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 02 Feb 2018 21:39:41 +0100

ros-rosdistro (0.6.3-1) unstable; urgency=medium

  * New upstream version 0.6.3

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 23 Dec 2017 15:51:19 +0100

ros-rosdistro (0.6.2-2) unstable; urgency=medium

  * Convert package to pybuild
  * Add python3-rosdistro package

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 14 Jul 2017 19:55:21 +0200

ros-rosdistro (0.6.2-1) unstable; urgency=medium

  * New upstream version 0.6.2
  * Update policy and debhelper versions
  * Update watch file

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 28 Jun 2017 06:04:43 +0200

ros-rosdistro (0.5.0-1) unstable; urgency=medium

  * Bumped Standards-Version to 3.9.8, no changes needed.
  * Update URLs
  * Update my email address
  * New upstream version 0.5.0
  * Update copyright

 -- Jochen Sprickerhof <jspricke@debian.org>  Fri, 04 Nov 2016 09:37:34 +0100

ros-rosdistro (0.4.7-1) unstable; urgency=medium

  * Imported Upstream version 0.4.7

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Sat, 18 Jun 2016 10:23:33 +0200

ros-rosdistro (0.4.4-1) unstable; urgency=medium

  * Imported Upstream version 0.4.4

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Thu, 11 Feb 2016 10:14:19 +0100

ros-rosdistro (0.4.3-1) unstable; urgency=medium

  * Add build-dep dh-python
  * Imported Upstream version 0.4.3

 -- Jochen Sprickerhof <debian@jochen.sprickerhof.de>  Fri, 05 Feb 2016 11:45:47 +0100

ros-rosdistro (0.4.2-1) unstable; urgency=medium

  [ Leopold Palomo-Avellaneda ]
  * New upstream version
  * Updating Description field

  [ Jochen Sprickerhof ]
  * Imported Upstream version 0.4.2
  * Prepared for Debian unstable (Closes: 804032)

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Thu, 12 Nov 2015 05:14:58 +0000

rosdistro (0.3.7-1.drp70+1) wheezy-robotics; urgency=medium

  * New upstream release.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Fri, 28 Nov 2014 15:21:21 +0100

rosdistro (0.3.4-1.drp70+1) wheezy-robotics; urgency=low

  * New upstream release.
  * Change nomeclature from +drp to .drp.

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Mon, 17 Feb 2014 12:01:14 +0100

rosdistro (0.2.14-1+drp70+1) wheezy-robotics; urgency=low

  * Rebuild for drp. First init package.

 -- Leopold Palomo-Avelleneda <leo@alaxarxa.net>  Thu, 10 Oct 2013 12:03:47 +0200

rosdistro (0.2.14-1) unstable; urgency=low

  * source package automatically created by stdeb 0.6.0+git

 -- Dirk Thomas <dthomas@osrfoundation.org>  Thu, 10 Oct 2013 11:59:22 +0200
